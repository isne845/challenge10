#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ){
		p=head->next;
		delete head;
		head=p;
	}
}

void List::headPush(int num) { //Head Push Function
	Node* h = new Node(num);
	if (head==0){
	 head = h;
	 tail = h;
	}
	else {
		h->next = head; 
		head->prev = h; //go to another way
		head =h;
	}
	}
void List::display(){ //display function
	Node* dp = head;
	while (dp != 0){
		cout << dp->info;
		dp = dp->next;
		cout << " ";
	}
}
void List::tailPush(int num){ //tail push function
	Node* t = new Node(num);
	tail->next =t;
	t->prev =tail; //doubly
	tail =  t;
	
}
int List::tailPop(){ //Tail pop function
	Node* p =tail;
	int store = tail->info; //store value before delete
	tail = tail->prev;//point to another way
	delete p;
	tail->next = 0;
	return store;
}
int List::headPop(){//head pop function
	Node* n = head;
	int store = head->info;
	head = head->next; //store value before  delete
	delete n;
	return store;
}

void List::deleteNode(int num){//delete function
	Node* d = head ,*d1, *d2;
	while(d->info != num){
		d = d->next;//point to previous node you want to delete
		}
		d1 = d->next;//point to the node you want to delete
		d2 = d->prev;//point to the next node you want to delete
		delete d;
		d2->next = d1;
		d1->prev = d2;//change the position of connected list
	
	}
	bool List::isInList(int num){ //found list function
		Node*f = head;
		while (f!=0){
			if(f != 0){
				if (f->info == num){
					return true;
				}
				else {
					f=f->next;
				}
			}
		}
		return false;
	}	

